import React, { useEffect, useState } from "react";
import SearchBox from "../SearchBox/SearchBox.component";
import Message from "../message-body/Message.component";
import { Accordion, Stack } from "react-bootstrap";
import CompetitorsData from "../../Competitor.data";
import Competitor from "../Competitor/Competitor.component";

function Page2() {
  const [selectedList, setSelectedList] = useState([]);
  const [activeAccord, setActiveAccord] = useState(0);

  // * Helper functions to help with id manip
  const addToSelectedId = (id) => setSelectedList([...selectedList, id]);
  const removeSelectedId = (id) =>
    setSelectedList(selectedList.filter((curr_id) => curr_id != id));

  // * helper functions for Array manipulation
  const diff = (a, b) => a.filter((elem) => !b.includes(elem.id));
  const neg = (a, b) => a.filter((elem) => b.includes(elem.id));

  // * checks when the list is switched and changes the selected accordingly
  useEffect(() => {
    setActiveAccord(selectedList.at(-1));
  }, [selectedList]);

  return (
    <Message position={1}>
      {/* Stack to add competitors */}
      <Stack className="w-50 overflow-scroll h-100">
        <h4 className="text-text-clr-3">Search For competitors</h4>
        <SearchBox />
        <Accordion className="overflow-scroll" activeKey={-1}>
          {/* Array is filtered to have the non selected ids */}
          {diff(CompetitorsData, selectedList).map((element) => (
            <Competitor
              id={element.id}
              title={element.name}
              key={element.id}
              onClick={() => addToSelectedId(element.id)}
              btnText="+"
            />
          ))}
        </Accordion>
      </Stack>

      {/* Stack to delete competitors */}
      <Stack className="w-50 overflow-scroll h-100">
        <h4 className="text-text-clr-3">Search For competitors</h4>
        <SearchBox />
        <Accordion className="overflow-scroll h-100" activeKey={activeAccord}>
          {/* Array is filtered to have the selected ids */}
          {neg(CompetitorsData, selectedList).map((element) => (
            <Competitor
              id={element.id}
              title={element.name}
              key={element.id}
              onBtnClick={() => removeSelectedId(element.id)}
              onClick={() => {
                setActiveAccord(element.id);
              }}
              btnText="-"
            />
          ))}
        </Accordion>
      </Stack>
    </Message>
  );
}

export default Page2;
