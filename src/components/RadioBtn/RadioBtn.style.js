import styled from "styled-components";

const RadioGrid = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 100%;

  & > label {
    align-items: center;
    display: flex;
    flex-basis: 75%;
    justify-content: space-between;
  }

  & > input:disabled ~ label {
    opacity: 50%;
  }
`;

export default RadioGrid;
