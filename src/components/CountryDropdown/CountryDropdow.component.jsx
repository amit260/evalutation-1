import React from "react";
import { Dropdown } from "react-bootstrap";

function CountryDropdown({ onSelect }) {
  const onClick = (value) => () => onSelect(value);
  return (
    <Dropdown>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        Dropdown Button
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item onClick={onClick("Country 1")}>Country 1</Dropdown.Item>
        <Dropdown.Item onClick={onClick("Country 2")}>Country 2</Dropdown.Item>
        <Dropdown.Item onClick={onClick("Country 3")}>Country 3</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default CountryDropdown;
