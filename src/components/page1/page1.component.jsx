import React from "react";
import Platform from "../platform/platform.component";
import SearchBox from "../SearchBox/SearchBox.component";
import { SubSection } from "../../Global.style";
import Message from "../message-body/Message.component";

function Page1() {
  return (
    <Message>
      <SubSection>
        <h4 className="text-text-clr-3">Select Platform</h4>
        <Platform />
      </SubSection>
      <SubSection>
        <h4 className="text-text-clr-3">Search App</h4>
        <SearchBox />
      </SubSection>
    </Message>
  );
}

export default Page1;
