import React from "react";
import { ReactComponent as Account } from "../../assets/person.svg";
import { ReactComponent as Exit } from "../../assets/exit.svg";
import { Navbar, Col } from "react-bootstrap";

function CustomNavbar() {
  return (
    <Navbar bg="primary" className="p-2">
      <Col className="d-flex justify-content-end">
        <Account />
        <Exit />
      </Col>
    </Navbar>
  );
}

export default CustomNavbar;
