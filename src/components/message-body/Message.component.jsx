import React from "react";
import StyledMessage from "./Message.style";
import { Stack, Row, Button } from "react-bootstrap";

/**
 * @description
 * It provides a wrapper for other components. It also provides
 * styles for the message.
 */
function Message({ children, position }) {
  return (
    <StyledMessage fluid="xl" className="bg-primary" position={position}>
      <Stack className="h-100 justify-content-between p-4">
        {/* Children Stack */}
        <Row className="justify-content-start text-center overflow-hidden">
          {children}
        </Row>

        {/* Button Controls */}
        <Stack className="justify-content-end" direction="horizontal" gap={3}>
          <Button variant="primary" className="text-text-clr-2">
            Skip
          </Button>
          <Button variant="text-clr-1" className="text-primary">
            Next
          </Button>
        </Stack>
      </Stack>
    </StyledMessage>
  );
}

export default Message;
