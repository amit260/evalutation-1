import styled from "styled-components";

export const SubSection = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  max-width: 300px;
  text-align: center;
  width: auto;
`;
