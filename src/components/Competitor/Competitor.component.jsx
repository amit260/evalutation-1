import React from "react";
import { Accordion, Button, Card, useAccordionButton } from "react-bootstrap";
import CompetitorDef from "../CompetitorDef/CompetitorDef.component";

function Competitor({ title, id, onClick, onBtnClick, btnText }) {
  return (
    <Accordion.Item eventKey={id}>
      <Accordion.Header onClick={onClick}>
        {title}
        <Button onClick={onBtnClick}>{btnText}</Button>
      </Accordion.Header>
      <Accordion.Body as="div">
        <div>
          <CompetitorDef />
        </div>
      </Accordion.Body>
    </Accordion.Item>
  );
}

export default Competitor;
