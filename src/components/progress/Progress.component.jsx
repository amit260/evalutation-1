import React from "react";
import { StyledProgress, StyledPoints } from "./progress.style";

function Progress({ position }) {
  return (
    <div className="container-xl bg-secondary my-3 py-4 px-10 mx-auto">
      <StyledProgress className="mx-auto">
        <StyledPoints active>
          <div></div>
          <h4>Add Your App</h4>
        </StyledPoints>
        <StyledPoints>
          <div></div>
          <h4>Add Competitors</h4>
        </StyledPoints>
        <StyledPoints>
          <div></div>
          <h4>App Keywords</h4>
        </StyledPoints>
      </StyledProgress>
    </div>
  );
}

export default Progress;
