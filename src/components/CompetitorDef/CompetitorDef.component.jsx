import React, { useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import CountryDropdown from "../CountryDropdown/CountryDropdow.component.jsx";

function CompetitorDef() {
  const [selected, setSelected] = useState([]);
  const addVal = (val) => setSelected([...selected, val]);

  return (
    <>
      <Row>
        <Col>
          {selected.map((val, index) => (
            <h4 key={index}>{val}</h4>
          ))}
        </Col>
        <Col>
          {selected.map((val, index) => (
            <Form.Check
              type="checkbox"
              label={`language ${index}`}
              key={index}
            />
          ))}
        </Col>
      </Row>
      <CountryDropdown onSelect={addVal} />
    </>
  );
}

export default CompetitorDef;
