import styled, { css } from "styled-components";
import { Container } from "react-bootstrap";

const StyledMessage = styled(Container)`
  height: 70vh;
  position: relative;

  &::after {
    background: white;
    clip-path: polygon(50% 0%, 0% 100%, 100% 100%);
    content: "";
    display: inline-block;
    height: 19px;
    left: ${({ position }) =>
      !position ? css`13%` : position === 1 ? css`48%` : css`83%`};
    top: 0;
    position: absolute;
    transform: translateY(-90%);
    width: 50px;
  }
`;

export default StyledMessage;
