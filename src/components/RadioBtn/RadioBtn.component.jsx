import React, { useRef, useState } from "react";
import RadioGrid from "./RadioBtn.style";
import { ReactComponent as Play } from "../../assets/play.svg";
import { ReactComponent as App } from "../../assets/app.svg";
import { Overlay, Tooltip } from "react-bootstrap";

function RadioBtn({ disabled, text, icon }) {
  const [showTooltip, setToolTip] = useState(false);
  const target = useRef(null);
  const handleClick = () => disabled && setToolTip(!showTooltip);

  return (
    <RadioGrid className="p-4" onClick={handleClick} ref={target}>
      <input
        className="form-check-input mx-2"
        type="radio"
        disabled={disabled}
      />
      <label>
        {text}
        <span>{!icon ? <Play /> : <App />}</span>
      </label>

      {/* Tooltip If disabled */}
      <Overlay target={target.current} show={showTooltip} placement="right">
        {(props) => (
          <Tooltip id="overlay-example" {...props}>
            ios support is coming soon
          </Tooltip>
        )}
      </Overlay>
    </RadioGrid>
  );
}

export default RadioBtn;
