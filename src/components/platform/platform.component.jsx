import React from "react";
import { Stack } from "react-bootstrap";
import RadioBtn from "../RadioBtn/RadioBtn.component";

function Platform() {
  return (
    <Stack className="bg-secondary ">
      <RadioBtn text={"Android"} />
      <RadioBtn text={"IOS"} icon={1} disabled />
    </Stack>
  );
}

export default Platform;
