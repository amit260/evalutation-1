import styled, { css } from "styled-components";

export const StyledProgress = styled.div`
  width: 75%;
  display: flex;
  justify-content: space-between;
  border-top: 2px solid black;
  padding-top: 15px;
  margin-top: 15px;
`;

export const StyledPoints = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  & > div {
    background-color: #ffffff;
    font-weight: bold;
    border: 2px solid black;
    margin-top: -25px;
    width: 19px;
    height: 18px;
    text-align: center;
    border-radius: 50%;
    ${(props) =>
      props.active &&
      css`
        background: black;
      `}
  }

  &:first-child {
    align-items: flex-start;
    & h4 {
      margin-left: -50%;
    }
  }

  &:last-child {
    align-items: flex-end;
    & h4 {
      margin-left: 0;
      margin-right: -50%;
    }
  }
`;
