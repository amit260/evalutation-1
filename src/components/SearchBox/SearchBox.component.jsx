import React from "react";
import { ReactComponent as Search } from "../../assets/search.svg";
import { Form, InputGroup } from "react-bootstrap";

function SearchBox() {
  return (
    <InputGroup>
      <InputGroup.Text className="bg-secondary">
        <Search />
      </InputGroup.Text>
      <Form.Control className="bg-secondary" />
    </InputGroup>
  );
}

export default SearchBox;
